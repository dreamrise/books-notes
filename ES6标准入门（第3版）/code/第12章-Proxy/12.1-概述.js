var obj = {
  count: 0,
  time: '2020-04-30'
}

var proxy = new Proxy(obj, {
  get: function (target, property) {
    console.log(target[property])
    return '2020-05-01'
  }
})

console.log(proxy.time)
console.log('----------')
console.log(obj.time)

var proxy1 = new Proxy(function (x, y) {
  return x + y;
}, {
  apply (target, object, args) {
    return args[0];
  }
})
console.log(proxy1(1, 2));	// 1