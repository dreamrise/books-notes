let thenable = {
  then: (resolve, reject) => {
    console.log(1);
    resolve(2);
  }
}

let p1 = Promise.resolve(thenable);
p1.then(value => console.log(value));
// 1
// 2