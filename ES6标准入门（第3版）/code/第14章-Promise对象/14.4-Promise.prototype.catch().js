new Promise((resolve, reject) => {
  reject(new Error('test'));
}).then(() => {
  console.log('resolve')
}, () => {
  console.log('reject1')
}).catch(() => {
  console.log('reject2')
})

new Promise((resolve, reject) => {
  resolve(1);
}).then(value => {
  console.log(value)
  throw new Error('test')
}).catch(err => {
  console.log('omg ' + err)
})