const target = {
  id: 'target'
}

const handler = {
  // 捕获器在处理程序对象中以方法名为键
  get () {
    return 'handler override'
  }
}

const proxy = new Proxy(target, handler);

console.log(target.id);
console.log(proxy.id);

console.log(target['id']);
console.log(proxy['id']);