const target = {
  foo: 'bar'
}

const handler = {
  get (trapTarget, property, receiver) {
    console.log(trapTarget === target);
    console.log(property);
    console.log(receiver === proxy);

    return trapTarget[property];
  }
}

const proxy = new Proxy(target, handler);

console.log(proxy.foo);