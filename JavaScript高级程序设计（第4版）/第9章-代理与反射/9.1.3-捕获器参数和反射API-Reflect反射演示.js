const target = {
  foo: 'bar',
  baz: 'qux'
}

const handler = {
  get (trapTarget, property, receiver) {
    let decoration = '';
    
    if (property === 'foo') {
      decoration = '!!!';
    }

    return Reflect.get(...arguments) + decoration;
  }
}

const proxy = new Proxy(target, handler);

console.log(target.foo);
console.log(proxy.foo);

console.log(target.baz);
console.log(proxy.baz);