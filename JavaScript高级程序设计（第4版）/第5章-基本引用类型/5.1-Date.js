// 符合预期，是本地时间日期对象
console.log(new Date(Date.UTC(2020, 11, 3, 9, 0, 0)));  // 2020-12-03T09:00:00.000Z

// 不符合预期，不是本地时间日期对象
console.log(new Date(Date.parse("December 3, 2020")));  // 2020-12-02T16:00:00.000Z
console.log(new Date("December 3, 2020"));              // 2020-12-02T16:00:00.000Z
console.log(new Date(2020, 11, 3, 9, 0, 0));            // 2020-12-03T01:00:00.000Z