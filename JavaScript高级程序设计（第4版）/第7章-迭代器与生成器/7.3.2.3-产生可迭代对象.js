function * commonGeneratorFn () {
  console.log('iter value: ', yield* [1, 2, 3]);
}

for (const x of commonGeneratorFn()) {
  console.log('value: ', x);
}
// value: 1
// value: 2
// value: 3
// iter value: undefined

function * innerGeneratorFn () {
  yield 'foo';
  return 'bar';
}

function * outerGeneratorFn () {
  console.log('iter value: ', yield* innerGeneratorFn());
}

for (const x of outerGeneratorFn()) {
  console.log('value: ', x);
}
// value: foo
// iter value: bar