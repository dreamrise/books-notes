let obj1 = {
  name: 'eagle',
  age: 21
}

let obj2 = obj1;

console.log(obj1 == obj2);
console.log(obj1 === obj2);

function test (obj3) {
  console.log(obj1 == obj3);
  console.log(obj1 === obj3);
}

test(obj1);