function SuperType (name) {
  this.name = name;
  this.colors = ['red', 'blue', 'green'];
}

SuperType.prototype.sayName = function () {
  console.log(this.name);
}

function SubType (name, age) {
  // 继承属性
  SuperType.call(this, name);

  this.age = age;
}

SubType.prototype = new SuperType;

SubType.prototype.sayAge = function () {
  console.log(this.age);
}

let instance1 = new SubType('eagle', 29);
instance1.colors.unshift('black');
console.log(instance1.colors);
instance1.sayAge();
instance1.sayName();

let instance2 = new SubType('sub-eagle', 22);
console.log(instance2.colors);
instance2.sayAge();
instance2.sayName();

console.log(instance1 instanceof SuperType);
console.log(instance1 instanceof SubType);

console.log(instance2 instanceof SuperType);
console.log(instance2 instanceof SubType);

console.log(SuperType.prototype.isPrototypeOf(instance1));
console.log(SubType.prototype.isPrototypeOf(instance1));

console.log(SuperType.prototype.isPrototypeOf(instance2));
console.log(SubType.prototype.isPrototypeOf(instance2));