let a = 6;
let b = 9;

// expressions 数组中包含着 a，b，a+b
function simpleTag (strings, ...expressions) {
  console.log(strings);
  for (const expression of expressions) {
    console.log(expression);
  }

  return 'foobar';
}

let taggedResult = simpleTag`flag1 ${a} + ${b} = ${a + b} flag2`;

console.log(taggedResult);