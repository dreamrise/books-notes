let s = new Set();
console.log(s);                         // Set (0) {}
console.log(s[Symbol.toStringTag]);     // Set
console.log(s.toString);                // [object Set]

class Foo {}
let foo = new Foo();
console.log(foo);                       // Foo {}
console.log(foo[Symbol.toStringTag]);   // undefined
console.log(foo.toString());            // [object Object]

class Bar {
  constructor () {
    this[Symbol.toStringTag] = 'Bar';
  }
}
let bar = new Bar();
console.log(bar);                       // Bar {}
console.log(bar[Symbol.toStringTag]);   // Bar
console.log(bar.toString());            // [object Bar]