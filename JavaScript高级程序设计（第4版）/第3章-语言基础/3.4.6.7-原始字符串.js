function printRaw (strings) {
  console.log('转义后的字符串：')
  for (const i in strings) {
    console.log(`第${Number(i) + 1}个参数：${strings[i]} ---`)
  }

  console.log('没有转义的原始字符串：');
  for (const rawString of strings.raw) {
    console.log(rawString);
  }
  
  console.log('标签函数第一个参数字符串数组的 raw 属性：', strings.raw);
}

printRaw`\u00A9${'test'}\n`;