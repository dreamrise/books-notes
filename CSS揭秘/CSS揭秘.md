# 第 1 章		引言

## CSS 编码技巧

### 尽量减少代码重复

灵活的 CSS 通常更容易扩展：在写出基础样式之后，只用极少的代码就可以扩展出不同的变体，因为只需要覆盖一些变量就可以了。下面是一个例子：

```css
padding: 6px 16px;
border: 1px solid #446d88;
background: #58a linear-gradient(#77a0bb, #58a);
border-radius: 4px;
box-shadow: 0 1px 5px gray;
color: white;
text-shadow: 0 -1px 1px #335166;
font-size: 20px;
line-height: 30px;
```

上面这段 CSS 给按钮添加了一些效果，但在可维护性方面存在一些问题。如果此时改变字号，生成一个更大的按钮，就得同时调整行高，因为两个属性都写成了绝对值，又因为行高没有反映出跟字号的关系，所以还得算出字号改变之后的行高是多少。**当某些值相互依赖时，应该把它们的相互关系用代码表示出来**，这个例子中，行高是字号的 1.5 倍，所以把代码改成下面这样更易维护：

```css
font-size: 20px;
line-height: 1.5;
```

既然已经把行高改为相对字号大小的值，那何不把字号的值也改为相对值而不是绝对值呢？绝对值很容器掌控，但是要修改时可能牵一发而动全身。像这里如果改变字号大小后，按钮中的文字会变大，但是按钮的其他效果并不会随着字号变大而变大，需要重新修改数值。如果改用百分比或 em 单位就好多了：

```css
padding: .3em .8em;
border: 1px solid #446d88;
background: #58a linear-gradient(#77a0bb, #58a);
border-radius: .2em;
box-shadow: 0 .05em .25em gray;
color: white;
text-shadow: 0 -.05em .05em #335166;
font-size: 125%;
line-height: 1.5;
```

上述样式中，只要改变按钮的字号大小或改变按钮父元素的字号大小，按钮整体都会发生比例变化。