let { x } = require('./2.5.2.2-export')

console.log(x)

// node 中的 CommonJS 方式导出的模块内容是可配置和可更改的
// console.log(Object.getOwnPropertyDescriptor(obj, 'x'))

// obj.x = 400

x = 400

console.log(x)