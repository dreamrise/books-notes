out_break:
for (let i = 0; i < 5; i++) {
  for (let j = 0; j < 3; j++) {
    console.log(i, j)
    if (i === 3) {
      break out_break;
    }
  }
}

console.log('===============')

out_continue:
for (let i = 0; i < 5; i++) {
  for (let j = 0; j < 3; j++) {
    console.log(i, j)
    if (i === 3) {
      continue out_continue;
    }
  }
}