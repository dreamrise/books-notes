const a = [ [1] [1] ]
console.log(a)  // [ undefined ]

// [1] [1] 被解析为取第一个数组 [ 1 ] 的第二个元素，而第二个元素不存在，所以返回 undefined

const a1 = [ [0, 1] [1] ]
console.log(a1) // [ 1 ]


const b = [ [1], [1] ]
console.log(b)

const c = [
  ['a', 1, 2, 3]  // 这里漏掉了逗号
  ['b', 4, 5, 6], // 逗号作为连续运算符，返回最后一个值 6，所以变为 ['a', 1, 2, 3][6]，也就返回 undefined
  ['c', 7, 8, 9]
]
console.log(c)  // [ undefined, [ 'c', 7, 8, 9 ] ]

const c1 = [
  ['a', 4, 5, 6]
  ['b', 1, 2, 3], // 逗号作为连续运算符，返回最后一个值 3，所以变为 ['a', 4, 5, 6][3]，也就会返回 6
  ['c', 7, 8, 9]
]
console.log(c1) // [ 6, [ 'c', 7, 8, 9 ] ]

const c2 = [
  ['a', 1, 2, 3]
  ['b', 4, 5, 6, 'length'],
  ['c', 7, 8, 9]
]
console.log(c2) // [ 4, [ 'c', 7, 8, 9 ] ]