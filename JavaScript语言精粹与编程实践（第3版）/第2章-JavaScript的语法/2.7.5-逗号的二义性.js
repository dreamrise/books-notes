var i = 100,
    print = x => console.log(x);

print((i += 20, i *= 2, 'value: ' + i)) // value: 240

i = 100

print(i += 20, i *= 2, 'value: ' + i) // 120