function tryFinally () {
  try {
    console.log('fun try')
    return 'eagle'
  } finally {
    console.log('fun finally')
  }
}

const funReturn = tryFinally()
console.log(funReturn)

out_break:
try {
  console.log('label try')
  break out_break
} finally {
  console.log('label finally')
}
console.log('label out')