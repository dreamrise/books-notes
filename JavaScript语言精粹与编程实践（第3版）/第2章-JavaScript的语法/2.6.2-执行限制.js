"use strict"

// 非严格模式下形式参数和 arguments 数组元素互相影响
// 严格模式下将不再互相影响
function foo (x) {
  console.log(x)
  console.log(arguments[0])

  x = 100

  console.log(x)
  console.log(arguments[0])

  arguments[0] = 200

  console.log(x)
  console.log(arguments[0])

  return x
}

console.log(foo(0))