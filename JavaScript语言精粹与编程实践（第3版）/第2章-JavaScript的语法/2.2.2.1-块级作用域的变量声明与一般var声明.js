var x = 100;
let y = 200;

/*
 *  node 环境下以下输出都是 undefined
 *  因为 node 中一个 js 文件就是一个完整的作用域模块
 *  而 global 是独立于所有 js 模块之上的
 *  浏览器环境下 global 对象是 window，则会 x 会正常输出，而 y 为 undefined
 */
console.log(Object.getOwnPropertyDescriptor(global, 'x'))
console.log(Object.getOwnPropertyDescriptor(global, 'y'))