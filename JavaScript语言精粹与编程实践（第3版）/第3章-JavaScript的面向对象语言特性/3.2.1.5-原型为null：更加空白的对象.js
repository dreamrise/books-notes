function MyObject () {
  this.showMe = function () {
    console.log(typeof this)
  }
  console.log('call')
}
MyObject.prototype = null

// 构造函数 prototype 是无效值如 null 或非对象的情况下，实例是通过 new Object() 来创建的
// 尽管实例是通过 new Object() 创建的，但 new MyObject 的 new 运算仍然会调用一次 MyObject
const obj = new MyObject  // call
console.log(Object.getPrototypeOf(obj).constructor === Object)  // true

obj.showMe()  // object

try {
  console.log(obj instanceof MyObject)
} catch ({ message }) {
  console.log(message)  // 打印错误消息
}

console.log(Object.getPrototypeOf(obj) === Object.prototype)  // true

Object.setPrototypeOf(obj, null)

console.log(typeof obj) // object
console.log('toString' in obj)  // false
console.log(obj instanceof Object)  // false
obj.showMe()  // object